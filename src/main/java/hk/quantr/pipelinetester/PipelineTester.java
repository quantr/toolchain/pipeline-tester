package hk.quantr.pipelinetester;

import hk.quantr.vcd.QuantrVCDLibrary;
import hk.quantr.vcd.datastructure.Data;
import hk.quantr.vcd.datastructure.Scope;
import hk.quantr.vcd.datastructure.VCD;
import hk.quantr.vcd.datastructure.Wire;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author abc
 */
public class PipelineTester {

    static String[] arg_buffer;
    static String symbol;

    public static void getScope(Scope scope) {
        for (Scope s : scope.scopes) {
            getScope(s);
//            System.out.println(s.name);
            if (s.name.equals(arg_buffer[0])) {
                for (Wire w : s.wires) {
                    if (w.name.equals(arg_buffer[1])) {
                        System.out.println(arg_buffer[0] + " -> " + arg_buffer[1] + " -> " + w.reference);
                        symbol = w.reference;
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        try {
            System.out.print("Your input: " + args[0] + "\t"); //scope name
            System.out.println(args[1]); //line no.
            arg_buffer = args[0].split("\\.");
            int line = Integer.parseInt(args[1]);
            int counter = 0;

            File folder = new File("../quantr-i");
            String fileContent = IOUtils.toString(new FileInputStream(folder.getAbsolutePath() + File.separator + "output.vcd"), "utf-8");
            VCD vcd = QuantrVCDLibrary.convertVCDToObject(folder, fileContent, false);

            getScope(vcd.scope);

            for (Data data : vcd.data) {
                String a = data.right;
                if (a != null && a.equals(symbol)) {
//                        System.out.println(data.left);
                    counter += 1;
                    if (counter == line) {
                        System.out.println(data.left);
                        String[] num = data.left.split("(?<=\\D)(?=\\d)");
                        //System.out.println(num[1]);
                        BigInteger value = BigInteger.valueOf(new BigInteger(num[1], 2).longValue());
                        //System.out.println(value.toString(16));
                        System.out.println(String.valueOf(value.toString(16)));

//                        int dec = Integer.parseInt(part[1], 2);
//                        System.out.println(Integer.toString(dec, 16));
                    }
                }
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(PipelineTester.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PipelineTester.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}
